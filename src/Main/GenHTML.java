package Main;

import java.io.FileNotFoundException;

import java.io.PrintWriter;

import java.util.ArrayList;


import WebPageCourses.Course;

import WebPageCourses.Coursework;
import WebPageCourses.Organization;
import WebPageCourses.StudyProgram;
import WebPageCourses.CreditReductionCourse;
import WebPageCourses.People;
import WebPageCourses.Schedule;
import WebPageCourses.EvaluationForm;
import WebPageCourses.Requirement;
import WebPageCourses.Work;
import WebPageCourses.RelatedCourses;
import WebPageCourses.Timetable;


public class GenHTML {
	
	
	ArrayList<String> genHtml(StudyProgram studyprogram) throws FileNotFoundException{
			ArrayList<String> strings = new ArrayList<String>();
			
		for(Course course : studyprogram.getCourses()) {
			strings.add(genCourse(course));
		}
		
			for(int i = 0; i < strings.size(); i++) {
				PrintWriter out = new PrintWriter("src/Main/WebPage" + i + ".html");
				out.println(strings.get(i));
				out.close();
			}
		
		return strings;
	}



	String genCourse(Course course) {
		String html = "<html>" + "<h1>"	+ course.getCode() + " " + course.getName()	+ "</h1>" + "<br>"
				+ genCourseWork(course)	+ genPeople(course) + genSchedule(course) + genWork(course)
				+ genCreditReduction(course) + genRequirement(course) + "</html>" ;

		return html;
	}

	
	
	String genCourseWork(Course course) {
		Coursework coursework = course.getCoursework();
		String table = "<div> <h2> Course Work </h2> <table border=\"1\">" +"<tr>" + "<th>"	+ "Lab Hours" + "</th>"
						+ "<th>" + "Lecture Hours" + " </th>" + "</tr>"	+ "<tr>" + "<th>" + coursework.getLabHours()
						+ "</th>" + "<th>" + coursework.getLectureHours() + "</th>"	+ "</tr>" +"</table> </div>";
		return table;
	}

	
	String genPeople(Course course) {
		Organization organization = course.getInstances().get(0).getOrganizations().get(0);
		String table = "<div> <h2> People </h2> <table border=\"1\">" + "<p>"  + organization.getDepartment()
						+ "</p>" +"<tr>" + "<th>" + "Name" + "</th>" + "<th>" + "Role" + " </th>" + "</tr>";
		for (People people : organization.getPeople()) {
			table += "<tr>"	+ "<th>" + people.getName()	+ "</th>" + "<th>" + people.getRole()+ "</th>"
				  + "</tr>";
		}
		table += "</table> </div>";
		return table;

	}
	
		
	String genSchedule(Course course) {
		Timetable timetable = course.getInstances().get(0).getTimetable();
		String table = "<div> <h2> Schedule </h2> <table border=\"1\">" +"<tr>"	+ "<th>"+ "Room" + "</th>"
						+ "<th>" + "Date" + " </th>" + "<th>" + "Time" + " </th>" + "</tr>";
			for (Schedule schedule : timetable.getSchedule()) {
			table += "<tr>"	+ "<th>" + schedule.getRoom() + "</th>" + "<th>" + schedule.getDate()
				  + "</th>" + "<th>" + schedule.getTime()	+ "</th>" + "</tr>";
			}
			table += "</table> </div>";
			return table;
		}

			
	String genWork(Course course) {
		EvaluationForm evaluation = course.getInstances().get(0).getEvaluationform();
		String table = "<div> <h2> Work </h2> <table border=\"1\">"	+"<tr>"	+ "<th>" + "Type" + "</th>"
							+ "<th>" + "Porcentage"	+ " </th>" + "</tr>";
		for (Work work : evaluation.getWork()) {
			table += "<tr>"	+ "<th>" + work.getName() + "</th>"	+ "<th>" + work.getPorcentage()
				  + "</th>" + "</tr>";
		}
		table += "</table> </div>";
		return table;

	}
	
		
	String genRequirement (Course course) {
		RelatedCourses related = course.getRelatedcourses();
		String table = "<div> <h2> Requirement Courses </h2> <table border=\"1\">" +"<tr>" + "<th>" + "Name"
						+ "</th>" + "<th>" + "Code" + " </th>" + "<th>" + "Type" + " </th>"	+ "</tr>";
		for (Requirement requirement : related.getRequiredcourses()) {
			table += "<tr>" + "<th>" + requirement.getName() + "</th>" + "<th>"	+ requirement.getCode()
				  + "</th>" + "<th>" + requirement.getTypeOfRequierement() + "</th>" + "</tr>";
		}
		table += "</table> </div>";
		return table;
	}
	
	
	String genCreditReduction (Course course) {
		RelatedCourses related = course.getRelatedcourses();
		String table = "<div> <h2> Credit Reduction Courses </h2> <table border=\"1\">"	+"<tr>" + "<th>" + "Code"
						+ "</th>" + "<th>" + "Credit Reduction"	+ " </th>" + "</tr>";
		for (CreditReductionCourse reduction : related.getReductioncourses()) {
			table += "<tr>"	+ "<th>" + reduction.getCode() + "</th>" + "<th>" + reduction.getCreditReduction()
				  + "</th>" + "</tr>";
		}
		table += "</table> </div>";
		return table;
	}
	
	
	
}
