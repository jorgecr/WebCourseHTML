package Main;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;



import WebPageCourses.StudyProgram;
import WebPageCourses.WebPageCoursesPackage;
import WebPageCourses.util.WebPageCoursesResourceFactoryImpl;


public class GenWebCourse {
	
	public static void main(String[] args) throws IOException {
		
		StudyProgram studyprogram = genStudy(args[0]);
		GenHTML generator = new GenHTML();
		generator.genHtml(studyprogram);
	}
		
	public static StudyProgram genStudy(String args) throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(WebPageCoursesPackage.eNS_URI, WebPageCoursesPackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new WebPageCoursesResourceFactoryImpl());
		Resource resource = resourceSet.getResource(URI.createFileURI(args), true);
			for (EObject eObject : resource.getContents()) {
				if (eObject instanceof StudyProgram) {
					return (StudyProgram) eObject;
				}
			}	
		
		return null;
	}
	
	
}
