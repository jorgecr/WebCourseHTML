/**
 */
package WebPageCourses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.Course#getInstances <em>Instances</em>}</li>
 *   <li>{@link WebPageCourses.Course#getCode <em>Code</em>}</li>
 *   <li>{@link WebPageCourses.Course#getName <em>Name</em>}</li>
 *   <li>{@link WebPageCourses.Course#getContent <em>Content</em>}</li>
 *   <li>{@link WebPageCourses.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link WebPageCourses.Course#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link WebPageCourses.Course#getRelatedcourses <em>Relatedcourses</em>}</li>
 *   <li>{@link WebPageCourses.Course#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore/OCL sameLectureHours='self.coursework.lectureHours &lt;= self.instances.timetable.lectureHours' sameLabHours='self.coursework.labHours &lt;= self.instances.timetable.labHours'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='sameLectureHours sameLabHours'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Instances</b></em>' containment reference list.
	 * The list contents are of type {@link WebPageCourses.CourseInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instances</em>' containment reference list.
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Instances()
	 * @model containment="true"
	 * @generated
	 */
	EList<CourseInstance> getInstances();

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Credits()
	 * @model
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Coursework</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coursework</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coursework</em>' containment reference.
	 * @see #setCoursework(Coursework)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Coursework()
	 * @model containment="true"
	 * @generated
	 */
	Coursework getCoursework();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getCoursework <em>Coursework</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coursework</em>' containment reference.
	 * @see #getCoursework()
	 * @generated
	 */
	void setCoursework(Coursework value);

	/**
	 * Returns the value of the '<em><b>Relatedcourses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relatedcourses</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relatedcourses</em>' containment reference.
	 * @see #setRelatedcourses(RelatedCourses)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Relatedcourses()
	 * @model containment="true"
	 * @generated
	 */
	RelatedCourses getRelatedcourses();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getRelatedcourses <em>Relatedcourses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relatedcourses</em>' containment reference.
	 * @see #getRelatedcourses()
	 * @generated
	 */
	void setRelatedcourses(RelatedCourses value);

	/**
	 * Returns the value of the '<em><b>Studyprograms</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link WebPageCourses.StudyProgram#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprograms</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprograms</em>' container reference.
	 * @see #setStudyprograms(StudyProgram)
	 * @see WebPageCourses.WebPageCoursesPackage#getCourse_Studyprograms()
	 * @see WebPageCourses.StudyProgram#getCourses
	 * @model opposite="courses" transient="false"
	 * @generated
	 */
	StudyProgram getStudyprograms();

	/**
	 * Sets the value of the '{@link WebPageCourses.Course#getStudyprograms <em>Studyprograms</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Studyprograms</em>' container reference.
	 * @see #getStudyprograms()
	 * @generated
	 */
	void setStudyprograms(StudyProgram value);

} // Course
