/**
 */
package WebPageCourses.impl;

import WebPageCourses.Course;
import WebPageCourses.CourseInstance;
import WebPageCourses.Coursework;
import WebPageCourses.RelatedCourses;
import WebPageCourses.StudyProgram;
import WebPageCourses.WebPageCoursesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getInstances <em>Instances</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getRelatedcourses <em>Relatedcourses</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseImpl#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The cached value of the '{@link #getInstances() <em>Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> instances;

	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCoursework() <em>Coursework</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoursework()
	 * @generated
	 * @ordered
	 */
	protected Coursework coursework;

	/**
	 * The cached value of the '{@link #getRelatedcourses() <em>Relatedcourses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedcourses()
	 * @generated
	 * @ordered
	 */
	protected RelatedCourses relatedcourses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getInstances() {
		if (instances == null) {
			instances = new EObjectContainmentEList<CourseInstance>(CourseInstance.class, this, WebPageCoursesPackage.COURSE__INSTANCES);
		}
		return instances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coursework getCoursework() {
		return coursework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoursework(Coursework newCoursework, NotificationChain msgs) {
		Coursework oldCoursework = coursework;
		coursework = newCoursework;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__COURSEWORK, oldCoursework, newCoursework);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoursework(Coursework newCoursework) {
		if (newCoursework != coursework) {
			NotificationChain msgs = null;
			if (coursework != null)
				msgs = ((InternalEObject)coursework).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE__COURSEWORK, null, msgs);
			if (newCoursework != null)
				msgs = ((InternalEObject)newCoursework).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE__COURSEWORK, null, msgs);
			msgs = basicSetCoursework(newCoursework, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__COURSEWORK, newCoursework, newCoursework));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelatedCourses getRelatedcourses() {
		return relatedcourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelatedcourses(RelatedCourses newRelatedcourses, NotificationChain msgs) {
		RelatedCourses oldRelatedcourses = relatedcourses;
		relatedcourses = newRelatedcourses;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__RELATEDCOURSES, oldRelatedcourses, newRelatedcourses);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelatedcourses(RelatedCourses newRelatedcourses) {
		if (newRelatedcourses != relatedcourses) {
			NotificationChain msgs = null;
			if (relatedcourses != null)
				msgs = ((InternalEObject)relatedcourses).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE__RELATEDCOURSES, null, msgs);
			if (newRelatedcourses != null)
				msgs = ((InternalEObject)newRelatedcourses).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE__RELATEDCOURSES, null, msgs);
			msgs = basicSetRelatedcourses(newRelatedcourses, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__RELATEDCOURSES, newRelatedcourses, newRelatedcourses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram getStudyprograms() {
		if (eContainerFeatureID() != WebPageCoursesPackage.COURSE__STUDYPROGRAMS) return null;
		return (StudyProgram)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStudyprograms(StudyProgram newStudyprograms, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStudyprograms, WebPageCoursesPackage.COURSE__STUDYPROGRAMS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStudyprograms(StudyProgram newStudyprograms) {
		if (newStudyprograms != eInternalContainer() || (eContainerFeatureID() != WebPageCoursesPackage.COURSE__STUDYPROGRAMS && newStudyprograms != null)) {
			if (EcoreUtil.isAncestor(this, newStudyprograms))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStudyprograms != null)
				msgs = ((InternalEObject)newStudyprograms).eInverseAdd(this, WebPageCoursesPackage.STUDY_PROGRAM__COURSES, StudyProgram.class, msgs);
			msgs = basicSetStudyprograms(newStudyprograms, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE__STUDYPROGRAMS, newStudyprograms, newStudyprograms));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStudyprograms((StudyProgram)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__INSTANCES:
				return ((InternalEList<?>)getInstances()).basicRemove(otherEnd, msgs);
			case WebPageCoursesPackage.COURSE__COURSEWORK:
				return basicSetCoursework(null, msgs);
			case WebPageCoursesPackage.COURSE__RELATEDCOURSES:
				return basicSetRelatedcourses(null, msgs);
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				return basicSetStudyprograms(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				return eInternalContainer().eInverseRemove(this, WebPageCoursesPackage.STUDY_PROGRAM__COURSES, StudyProgram.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__INSTANCES:
				return getInstances();
			case WebPageCoursesPackage.COURSE__CODE:
				return getCode();
			case WebPageCoursesPackage.COURSE__NAME:
				return getName();
			case WebPageCoursesPackage.COURSE__CONTENT:
				return getContent();
			case WebPageCoursesPackage.COURSE__CREDITS:
				return getCredits();
			case WebPageCoursesPackage.COURSE__COURSEWORK:
				return getCoursework();
			case WebPageCoursesPackage.COURSE__RELATEDCOURSES:
				return getRelatedcourses();
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				return getStudyprograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__INSTANCES:
				getInstances().clear();
				getInstances().addAll((Collection<? extends CourseInstance>)newValue);
				return;
			case WebPageCoursesPackage.COURSE__CODE:
				setCode((String)newValue);
				return;
			case WebPageCoursesPackage.COURSE__NAME:
				setName((String)newValue);
				return;
			case WebPageCoursesPackage.COURSE__CONTENT:
				setContent((String)newValue);
				return;
			case WebPageCoursesPackage.COURSE__CREDITS:
				setCredits((Double)newValue);
				return;
			case WebPageCoursesPackage.COURSE__COURSEWORK:
				setCoursework((Coursework)newValue);
				return;
			case WebPageCoursesPackage.COURSE__RELATEDCOURSES:
				setRelatedcourses((RelatedCourses)newValue);
				return;
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				setStudyprograms((StudyProgram)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__INSTANCES:
				getInstances().clear();
				return;
			case WebPageCoursesPackage.COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSE__CONTENT:
				setContent(CONTENT_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSE__CREDITS:
				setCredits(CREDITS_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSE__COURSEWORK:
				setCoursework((Coursework)null);
				return;
			case WebPageCoursesPackage.COURSE__RELATEDCOURSES:
				setRelatedcourses((RelatedCourses)null);
				return;
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				setStudyprograms((StudyProgram)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE__INSTANCES:
				return instances != null && !instances.isEmpty();
			case WebPageCoursesPackage.COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case WebPageCoursesPackage.COURSE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case WebPageCoursesPackage.COURSE__CONTENT:
				return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
			case WebPageCoursesPackage.COURSE__CREDITS:
				return credits != CREDITS_EDEFAULT;
			case WebPageCoursesPackage.COURSE__COURSEWORK:
				return coursework != null;
			case WebPageCoursesPackage.COURSE__RELATEDCOURSES:
				return relatedcourses != null;
			case WebPageCoursesPackage.COURSE__STUDYPROGRAMS:
				return getStudyprograms() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
