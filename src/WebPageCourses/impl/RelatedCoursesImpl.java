/**
 */
package WebPageCourses.impl;

import WebPageCourses.CreditReductionCourse;
import WebPageCourses.RelatedCourses;
import WebPageCourses.Requirement;
import WebPageCourses.WebPageCoursesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Related Courses</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.RelatedCoursesImpl#getRequiredcourses <em>Requiredcourses</em>}</li>
 *   <li>{@link WebPageCourses.impl.RelatedCoursesImpl#getReductioncourses <em>Reductioncourses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelatedCoursesImpl extends MinimalEObjectImpl.Container implements RelatedCourses {
	/**
	 * The cached value of the '{@link #getRequiredcourses() <em>Requiredcourses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredcourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Requirement> requiredcourses;

	/**
	 * The cached value of the '{@link #getReductioncourses() <em>Reductioncourses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReductioncourses()
	 * @generated
	 * @ordered
	 */
	protected EList<CreditReductionCourse> reductioncourses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelatedCoursesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.RELATED_COURSES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Requirement> getRequiredcourses() {
		if (requiredcourses == null) {
			requiredcourses = new EObjectContainmentEList<Requirement>(Requirement.class, this, WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES);
		}
		return requiredcourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CreditReductionCourse> getReductioncourses() {
		if (reductioncourses == null) {
			reductioncourses = new EObjectContainmentEList<CreditReductionCourse>(CreditReductionCourse.class, this, WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES);
		}
		return reductioncourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES:
				return ((InternalEList<?>)getRequiredcourses()).basicRemove(otherEnd, msgs);
			case WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES:
				return ((InternalEList<?>)getReductioncourses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES:
				return getRequiredcourses();
			case WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES:
				return getReductioncourses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES:
				getRequiredcourses().clear();
				getRequiredcourses().addAll((Collection<? extends Requirement>)newValue);
				return;
			case WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES:
				getReductioncourses().clear();
				getReductioncourses().addAll((Collection<? extends CreditReductionCourse>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES:
				getRequiredcourses().clear();
				return;
			case WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES:
				getReductioncourses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.RELATED_COURSES__REQUIREDCOURSES:
				return requiredcourses != null && !requiredcourses.isEmpty();
			case WebPageCoursesPackage.RELATED_COURSES__REDUCTIONCOURSES:
				return reductioncourses != null && !reductioncourses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RelatedCoursesImpl
