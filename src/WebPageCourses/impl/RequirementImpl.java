/**
 */
package WebPageCourses.impl;

import WebPageCourses.Requirement;
import WebPageCourses.WebPageCoursesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.RequirementImpl#getCode <em>Code</em>}</li>
 *   <li>{@link WebPageCourses.impl.RequirementImpl#getName <em>Name</em>}</li>
 *   <li>{@link WebPageCourses.impl.RequirementImpl#getTypeOfRequierement <em>Type Of Requierement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementImpl extends MinimalEObjectImpl.Container implements Requirement {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeOfRequierement() <em>Type Of Requierement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOfRequierement()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_OF_REQUIEREMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeOfRequierement() <em>Type Of Requierement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeOfRequierement()
	 * @generated
	 * @ordered
	 */
	protected String typeOfRequierement = TYPE_OF_REQUIEREMENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.REQUIREMENT__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.REQUIREMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeOfRequierement() {
		return typeOfRequierement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeOfRequierement(String newTypeOfRequierement) {
		String oldTypeOfRequierement = typeOfRequierement;
		typeOfRequierement = newTypeOfRequierement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.REQUIREMENT__TYPE_OF_REQUIEREMENT, oldTypeOfRequierement, typeOfRequierement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.REQUIREMENT__CODE:
				return getCode();
			case WebPageCoursesPackage.REQUIREMENT__NAME:
				return getName();
			case WebPageCoursesPackage.REQUIREMENT__TYPE_OF_REQUIEREMENT:
				return getTypeOfRequierement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.REQUIREMENT__CODE:
				setCode((String)newValue);
				return;
			case WebPageCoursesPackage.REQUIREMENT__NAME:
				setName((String)newValue);
				return;
			case WebPageCoursesPackage.REQUIREMENT__TYPE_OF_REQUIEREMENT:
				setTypeOfRequierement((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.REQUIREMENT__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case WebPageCoursesPackage.REQUIREMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case WebPageCoursesPackage.REQUIREMENT__TYPE_OF_REQUIEREMENT:
				setTypeOfRequierement(TYPE_OF_REQUIEREMENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.REQUIREMENT__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case WebPageCoursesPackage.REQUIREMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case WebPageCoursesPackage.REQUIREMENT__TYPE_OF_REQUIEREMENT:
				return TYPE_OF_REQUIEREMENT_EDEFAULT == null ? typeOfRequierement != null : !TYPE_OF_REQUIEREMENT_EDEFAULT.equals(typeOfRequierement);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", TypeOfRequierement: ");
		result.append(typeOfRequierement);
		result.append(')');
		return result.toString();
	}

} //RequirementImpl
