/**
 */
package WebPageCourses.impl;

import WebPageCourses.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WebPageCoursesFactoryImpl extends EFactoryImpl implements WebPageCoursesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WebPageCoursesFactory init() {
		try {
			WebPageCoursesFactory theWebPageCoursesFactory = (WebPageCoursesFactory)EPackage.Registry.INSTANCE.getEFactory(WebPageCoursesPackage.eNS_URI);
			if (theWebPageCoursesFactory != null) {
				return theWebPageCoursesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WebPageCoursesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebPageCoursesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WebPageCoursesPackage.COURSE: return createCourse();
			case WebPageCoursesPackage.COURSE_INSTANCE: return createCourseInstance();
			case WebPageCoursesPackage.PEOPLE: return createPeople();
			case WebPageCoursesPackage.REQUIREMENT: return createRequirement();
			case WebPageCoursesPackage.EVALUATION_FORM: return createEvaluationForm();
			case WebPageCoursesPackage.WORK: return createWork();
			case WebPageCoursesPackage.STUDY_PROGRAM: return createStudyProgram();
			case WebPageCoursesPackage.COURSEWORK: return createCoursework();
			case WebPageCoursesPackage.ORGANIZATION: return createOrganization();
			case WebPageCoursesPackage.TIMETABLE: return createTimetable();
			case WebPageCoursesPackage.RELATED_COURSES: return createRelatedCourses();
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE: return createCreditReductionCourse();
			case WebPageCoursesPackage.SCHEDULE: return createSchedule();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance createCourseInstance() {
		CourseInstanceImpl courseInstance = new CourseInstanceImpl();
		return courseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public People createPeople() {
		PeopleImpl people = new PeopleImpl();
		return people;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement createRequirement() {
		RequirementImpl requirement = new RequirementImpl();
		return requirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationForm createEvaluationForm() {
		EvaluationFormImpl evaluationForm = new EvaluationFormImpl();
		return evaluationForm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Work createWork() {
		WorkImpl work = new WorkImpl();
		return work;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram createStudyProgram() {
		StudyProgramImpl studyProgram = new StudyProgramImpl();
		return studyProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coursework createCoursework() {
		CourseworkImpl coursework = new CourseworkImpl();
		return coursework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Organization createOrganization() {
		OrganizationImpl organization = new OrganizationImpl();
		return organization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable createTimetable() {
		TimetableImpl timetable = new TimetableImpl();
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelatedCourses createRelatedCourses() {
		RelatedCoursesImpl relatedCourses = new RelatedCoursesImpl();
		return relatedCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreditReductionCourse createCreditReductionCourse() {
		CreditReductionCourseImpl creditReductionCourse = new CreditReductionCourseImpl();
		return creditReductionCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schedule createSchedule() {
		ScheduleImpl schedule = new ScheduleImpl();
		return schedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebPageCoursesPackage getWebPageCoursesPackage() {
		return (WebPageCoursesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WebPageCoursesPackage getPackage() {
		return WebPageCoursesPackage.eINSTANCE;
	}

} //WebPageCoursesFactoryImpl
