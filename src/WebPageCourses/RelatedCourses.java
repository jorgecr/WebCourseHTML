/**
 */
package WebPageCourses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Related Courses</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.RelatedCourses#getRequiredcourses <em>Requiredcourses</em>}</li>
 *   <li>{@link WebPageCourses.RelatedCourses#getReductioncourses <em>Reductioncourses</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getRelatedCourses()
 * @model
 * @generated
 */
public interface RelatedCourses extends EObject {
	/**
	 * Returns the value of the '<em><b>Requiredcourses</b></em>' containment reference list.
	 * The list contents are of type {@link WebPageCourses.Requirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requiredcourses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requiredcourses</em>' containment reference list.
	 * @see WebPageCourses.WebPageCoursesPackage#getRelatedCourses_Requiredcourses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Requirement> getRequiredcourses();

	/**
	 * Returns the value of the '<em><b>Reductioncourses</b></em>' containment reference list.
	 * The list contents are of type {@link WebPageCourses.CreditReductionCourse}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reductioncourses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reductioncourses</em>' containment reference list.
	 * @see WebPageCourses.WebPageCoursesPackage#getRelatedCourses_Reductioncourses()
	 * @model containment="true"
	 * @generated
	 */
	EList<CreditReductionCourse> getReductioncourses();

} // RelatedCourses
