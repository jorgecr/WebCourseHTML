/**
 */
package WebPageCourses;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see WebPageCourses.WebPageCoursesFactory
 * @model kind="package"
 * @generated
 */
public interface WebPageCoursesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "WebPageCourses";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/CourseWebPage/model/coursewebpage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "coursewebpage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WebPageCoursesPackage eINSTANCE = WebPageCourses.impl.WebPageCoursesPackageImpl.init();

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.CourseImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__INSTANCES = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 3;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 4;

	/**
	 * The feature id for the '<em><b>Coursework</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSEWORK = 5;

	/**
	 * The feature id for the '<em><b>Relatedcourses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RELATEDCOURSES = 6;

	/**
	 * The feature id for the '<em><b>Studyprograms</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__STUDYPROGRAMS = 7;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.CourseInstanceImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCourseInstance()
	 * @generated
	 */
	int COURSE_INSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__SEMESTER = 0;

	/**
	 * The feature id for the '<em><b>Evaluationform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__EVALUATIONFORM = 1;

	/**
	 * The feature id for the '<em><b>Organizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__ORGANIZATIONS = 2;

	/**
	 * The feature id for the '<em><b>Timetable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE__TIMETABLE = 3;

	/**
	 * The number of structural features of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Course Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.PeopleImpl <em>People</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.PeopleImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getPeople()
	 * @generated
	 */
	int PEOPLE = 2;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__ROLE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__NAME = 1;

	/**
	 * The number of structural features of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.RequirementImpl <em>Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.RequirementImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getRequirement()
	 * @generated
	 */
	int REQUIREMENT = 3;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Type Of Requierement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__TYPE_OF_REQUIEREMENT = 2;

	/**
	 * The number of structural features of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.EvaluationFormImpl <em>Evaluation Form</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.EvaluationFormImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getEvaluationForm()
	 * @generated
	 */
	int EVALUATION_FORM = 4;

	/**
	 * The feature id for the '<em><b>Work</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM__WORK = 0;

	/**
	 * The number of structural features of the '<em>Evaluation Form</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Evaluation Form</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_FORM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.WorkImpl <em>Work</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.WorkImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getWork()
	 * @generated
	 */
	int WORK = 5;

	/**
	 * The feature id for the '<em><b>Porcentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__PORCENTAGE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK__NAME = 1;

	/**
	 * The number of structural features of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Work</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.StudyProgramImpl <em>Study Program</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.StudyProgramImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getStudyProgram()
	 * @generated
	 */
	int STUDY_PROGRAM = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__SCHEDULE = 1;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM__COURSES = 2;

	/**
	 * The number of structural features of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Study Program</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.CourseworkImpl <em>Coursework</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.CourseworkImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCoursework()
	 * @generated
	 */
	int COURSEWORK = 7;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LAB_HOURS = 1;

	/**
	 * The number of structural features of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.OrganizationImpl <em>Organization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.OrganizationImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getOrganization()
	 * @generated
	 */
	int ORGANIZATION = 8;

	/**
	 * The feature id for the '<em><b>Department</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__DEPARTMENT = 0;

	/**
	 * The feature id for the '<em><b>People</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION__PEOPLE = 1;

	/**
	 * The number of structural features of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Organization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANIZATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.TimetableImpl <em>Timetable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.TimetableImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getTimetable()
	 * @generated
	 */
	int TIMETABLE = 9;

	/**
	 * The feature id for the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__LECTURE_HOURS = 0;

	/**
	 * The feature id for the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__LAB_HOURS = 1;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__SCHEDULE = 2;

	/**
	 * The number of structural features of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.RelatedCoursesImpl <em>Related Courses</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.RelatedCoursesImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getRelatedCourses()
	 * @generated
	 */
	int RELATED_COURSES = 10;

	/**
	 * The feature id for the '<em><b>Requiredcourses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_COURSES__REQUIREDCOURSES = 0;

	/**
	 * The feature id for the '<em><b>Reductioncourses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_COURSES__REDUCTIONCOURSES = 1;

	/**
	 * The number of structural features of the '<em>Related Courses</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_COURSES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Related Courses</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATED_COURSES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.CreditReductionCourseImpl <em>Credit Reduction Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.CreditReductionCourseImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCreditReductionCourse()
	 * @generated
	 */
	int CREDIT_REDUCTION_COURSE = 11;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Credit Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION = 1;

	/**
	 * The number of structural features of the '<em>Credit Reduction Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_COURSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Credit Reduction Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDIT_REDUCTION_COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link WebPageCourses.impl.ScheduleImpl <em>Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see WebPageCourses.impl.ScheduleImpl
	 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getSchedule()
	 * @generated
	 */
	int SCHEDULE = 12;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__DATE = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__TIME = 1;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__ROOM = 2;

	/**
	 * The number of structural features of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link WebPageCourses.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see WebPageCourses.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.Course#getInstances <em>Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instances</em>'.
	 * @see WebPageCourses.Course#getInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Instances();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see WebPageCourses.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see WebPageCourses.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see WebPageCourses.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see WebPageCourses.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the containment reference '{@link WebPageCourses.Course#getCoursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Coursework</em>'.
	 * @see WebPageCourses.Course#getCoursework()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Coursework();

	/**
	 * Returns the meta object for the containment reference '{@link WebPageCourses.Course#getRelatedcourses <em>Relatedcourses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Relatedcourses</em>'.
	 * @see WebPageCourses.Course#getRelatedcourses()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Relatedcourses();

	/**
	 * Returns the meta object for the container reference '{@link WebPageCourses.Course#getStudyprograms <em>Studyprograms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Studyprograms</em>'.
	 * @see WebPageCourses.Course#getStudyprograms()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Studyprograms();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.CourseInstance <em>Course Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Instance</em>'.
	 * @see WebPageCourses.CourseInstance
	 * @generated
	 */
	EClass getCourseInstance();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.CourseInstance#getSemester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semester</em>'.
	 * @see WebPageCourses.CourseInstance#getSemester()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EAttribute getCourseInstance_Semester();

	/**
	 * Returns the meta object for the containment reference '{@link WebPageCourses.CourseInstance#getEvaluationform <em>Evaluationform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Evaluationform</em>'.
	 * @see WebPageCourses.CourseInstance#getEvaluationform()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Evaluationform();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.CourseInstance#getOrganizations <em>Organizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Organizations</em>'.
	 * @see WebPageCourses.CourseInstance#getOrganizations()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Organizations();

	/**
	 * Returns the meta object for the containment reference '{@link WebPageCourses.CourseInstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timetable</em>'.
	 * @see WebPageCourses.CourseInstance#getTimetable()
	 * @see #getCourseInstance()
	 * @generated
	 */
	EReference getCourseInstance_Timetable();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.People <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>People</em>'.
	 * @see WebPageCourses.People
	 * @generated
	 */
	EClass getPeople();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.People#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see WebPageCourses.People#getRole()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Role();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.People#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see WebPageCourses.People#getName()
	 * @see #getPeople()
	 * @generated
	 */
	EAttribute getPeople_Name();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement</em>'.
	 * @see WebPageCourses.Requirement
	 * @generated
	 */
	EClass getRequirement();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Requirement#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see WebPageCourses.Requirement#getCode()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_Code();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Requirement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see WebPageCourses.Requirement#getName()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_Name();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Requirement#getTypeOfRequierement <em>Type Of Requierement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Of Requierement</em>'.
	 * @see WebPageCourses.Requirement#getTypeOfRequierement()
	 * @see #getRequirement()
	 * @generated
	 */
	EAttribute getRequirement_TypeOfRequierement();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.EvaluationForm <em>Evaluation Form</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Form</em>'.
	 * @see WebPageCourses.EvaluationForm
	 * @generated
	 */
	EClass getEvaluationForm();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.EvaluationForm#getWork <em>Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Work</em>'.
	 * @see WebPageCourses.EvaluationForm#getWork()
	 * @see #getEvaluationForm()
	 * @generated
	 */
	EReference getEvaluationForm_Work();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Work <em>Work</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Work</em>'.
	 * @see WebPageCourses.Work
	 * @generated
	 */
	EClass getWork();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Work#getPorcentage <em>Porcentage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Porcentage</em>'.
	 * @see WebPageCourses.Work#getPorcentage()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Porcentage();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Work#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see WebPageCourses.Work#getName()
	 * @see #getWork()
	 * @generated
	 */
	EAttribute getWork_Name();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.StudyProgram <em>Study Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Program</em>'.
	 * @see WebPageCourses.StudyProgram
	 * @generated
	 */
	EClass getStudyProgram();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.StudyProgram#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see WebPageCourses.StudyProgram#getName()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EAttribute getStudyProgram_Name();

	/**
	 * Returns the meta object for the reference list '{@link WebPageCourses.StudyProgram#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Schedule</em>'.
	 * @see WebPageCourses.StudyProgram#getSchedule()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Schedule();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.StudyProgram#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see WebPageCourses.StudyProgram#getCourses()
	 * @see #getStudyProgram()
	 * @generated
	 */
	EReference getStudyProgram_Courses();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Coursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coursework</em>'.
	 * @see WebPageCourses.Coursework
	 * @generated
	 */
	EClass getCoursework();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Coursework#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see WebPageCourses.Coursework#getLectureHours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Coursework#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see WebPageCourses.Coursework#getLabHours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_LabHours();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Organization <em>Organization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organization</em>'.
	 * @see WebPageCourses.Organization
	 * @generated
	 */
	EClass getOrganization();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Organization#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Department</em>'.
	 * @see WebPageCourses.Organization#getDepartment()
	 * @see #getOrganization()
	 * @generated
	 */
	EAttribute getOrganization_Department();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.Organization#getPeople <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>People</em>'.
	 * @see WebPageCourses.Organization#getPeople()
	 * @see #getOrganization()
	 * @generated
	 */
	EReference getOrganization_People();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Timetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timetable</em>'.
	 * @see WebPageCourses.Timetable
	 * @generated
	 */
	EClass getTimetable();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Timetable#getLectureHours <em>Lecture Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecture Hours</em>'.
	 * @see WebPageCourses.Timetable#getLectureHours()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_LectureHours();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Timetable#getLabHours <em>Lab Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lab Hours</em>'.
	 * @see WebPageCourses.Timetable#getLabHours()
	 * @see #getTimetable()
	 * @generated
	 */
	EAttribute getTimetable_LabHours();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.Timetable#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schedule</em>'.
	 * @see WebPageCourses.Timetable#getSchedule()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_Schedule();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.RelatedCourses <em>Related Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Related Courses</em>'.
	 * @see WebPageCourses.RelatedCourses
	 * @generated
	 */
	EClass getRelatedCourses();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.RelatedCourses#getRequiredcourses <em>Requiredcourses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requiredcourses</em>'.
	 * @see WebPageCourses.RelatedCourses#getRequiredcourses()
	 * @see #getRelatedCourses()
	 * @generated
	 */
	EReference getRelatedCourses_Requiredcourses();

	/**
	 * Returns the meta object for the containment reference list '{@link WebPageCourses.RelatedCourses#getReductioncourses <em>Reductioncourses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reductioncourses</em>'.
	 * @see WebPageCourses.RelatedCourses#getReductioncourses()
	 * @see #getRelatedCourses()
	 * @generated
	 */
	EReference getRelatedCourses_Reductioncourses();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.CreditReductionCourse <em>Credit Reduction Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Credit Reduction Course</em>'.
	 * @see WebPageCourses.CreditReductionCourse
	 * @generated
	 */
	EClass getCreditReductionCourse();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.CreditReductionCourse#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see WebPageCourses.CreditReductionCourse#getCode()
	 * @see #getCreditReductionCourse()
	 * @generated
	 */
	EAttribute getCreditReductionCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.CreditReductionCourse#getCreditReduction <em>Credit Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credit Reduction</em>'.
	 * @see WebPageCourses.CreditReductionCourse#getCreditReduction()
	 * @see #getCreditReductionCourse()
	 * @generated
	 */
	EAttribute getCreditReductionCourse_CreditReduction();

	/**
	 * Returns the meta object for class '{@link WebPageCourses.Schedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule</em>'.
	 * @see WebPageCourses.Schedule
	 * @generated
	 */
	EClass getSchedule();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Schedule#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see WebPageCourses.Schedule#getDate()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Date();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Schedule#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see WebPageCourses.Schedule#getTime()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Time();

	/**
	 * Returns the meta object for the attribute '{@link WebPageCourses.Schedule#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see WebPageCourses.Schedule#getRoom()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Room();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WebPageCoursesFactory getWebPageCoursesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.CourseImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__INSTANCES = eINSTANCE.getCourse_Instances();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Coursework</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSEWORK = eINSTANCE.getCourse_Coursework();

		/**
		 * The meta object literal for the '<em><b>Relatedcourses</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RELATEDCOURSES = eINSTANCE.getCourse_Relatedcourses();

		/**
		 * The meta object literal for the '<em><b>Studyprograms</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__STUDYPROGRAMS = eINSTANCE.getCourse_Studyprograms();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.CourseInstanceImpl <em>Course Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.CourseInstanceImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCourseInstance()
		 * @generated
		 */
		EClass COURSE_INSTANCE = eINSTANCE.getCourseInstance();

		/**
		 * The meta object literal for the '<em><b>Semester</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_INSTANCE__SEMESTER = eINSTANCE.getCourseInstance_Semester();

		/**
		 * The meta object literal for the '<em><b>Evaluationform</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__EVALUATIONFORM = eINSTANCE.getCourseInstance_Evaluationform();

		/**
		 * The meta object literal for the '<em><b>Organizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__ORGANIZATIONS = eINSTANCE.getCourseInstance_Organizations();

		/**
		 * The meta object literal for the '<em><b>Timetable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_INSTANCE__TIMETABLE = eINSTANCE.getCourseInstance_Timetable();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.PeopleImpl <em>People</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.PeopleImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getPeople()
		 * @generated
		 */
		EClass PEOPLE = eINSTANCE.getPeople();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__ROLE = eINSTANCE.getPeople_Role();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PEOPLE__NAME = eINSTANCE.getPeople_Name();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.RequirementImpl <em>Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.RequirementImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getRequirement()
		 * @generated
		 */
		EClass REQUIREMENT = eINSTANCE.getRequirement();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__CODE = eINSTANCE.getRequirement_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__NAME = eINSTANCE.getRequirement_Name();

		/**
		 * The meta object literal for the '<em><b>Type Of Requierement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIREMENT__TYPE_OF_REQUIEREMENT = eINSTANCE.getRequirement_TypeOfRequierement();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.EvaluationFormImpl <em>Evaluation Form</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.EvaluationFormImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getEvaluationForm()
		 * @generated
		 */
		EClass EVALUATION_FORM = eINSTANCE.getEvaluationForm();

		/**
		 * The meta object literal for the '<em><b>Work</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_FORM__WORK = eINSTANCE.getEvaluationForm_Work();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.WorkImpl <em>Work</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.WorkImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getWork()
		 * @generated
		 */
		EClass WORK = eINSTANCE.getWork();

		/**
		 * The meta object literal for the '<em><b>Porcentage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__PORCENTAGE = eINSTANCE.getWork_Porcentage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORK__NAME = eINSTANCE.getWork_Name();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.StudyProgramImpl <em>Study Program</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.StudyProgramImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getStudyProgram()
		 * @generated
		 */
		EClass STUDY_PROGRAM = eINSTANCE.getStudyProgram();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAM__NAME = eINSTANCE.getStudyProgram_Name();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__SCHEDULE = eINSTANCE.getStudyProgram_Schedule();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAM__COURSES = eINSTANCE.getStudyProgram_Courses();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.CourseworkImpl <em>Coursework</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.CourseworkImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCoursework()
		 * @generated
		 */
		EClass COURSEWORK = eINSTANCE.getCoursework();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LECTURE_HOURS = eINSTANCE.getCoursework_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LAB_HOURS = eINSTANCE.getCoursework_LabHours();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.OrganizationImpl <em>Organization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.OrganizationImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getOrganization()
		 * @generated
		 */
		EClass ORGANIZATION = eINSTANCE.getOrganization();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANIZATION__DEPARTMENT = eINSTANCE.getOrganization_Department();

		/**
		 * The meta object literal for the '<em><b>People</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANIZATION__PEOPLE = eINSTANCE.getOrganization_People();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.TimetableImpl <em>Timetable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.TimetableImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getTimetable()
		 * @generated
		 */
		EClass TIMETABLE = eINSTANCE.getTimetable();

		/**
		 * The meta object literal for the '<em><b>Lecture Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__LECTURE_HOURS = eINSTANCE.getTimetable_LectureHours();

		/**
		 * The meta object literal for the '<em><b>Lab Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMETABLE__LAB_HOURS = eINSTANCE.getTimetable_LabHours();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__SCHEDULE = eINSTANCE.getTimetable_Schedule();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.RelatedCoursesImpl <em>Related Courses</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.RelatedCoursesImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getRelatedCourses()
		 * @generated
		 */
		EClass RELATED_COURSES = eINSTANCE.getRelatedCourses();

		/**
		 * The meta object literal for the '<em><b>Requiredcourses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATED_COURSES__REQUIREDCOURSES = eINSTANCE.getRelatedCourses_Requiredcourses();

		/**
		 * The meta object literal for the '<em><b>Reductioncourses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATED_COURSES__REDUCTIONCOURSES = eINSTANCE.getRelatedCourses_Reductioncourses();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.CreditReductionCourseImpl <em>Credit Reduction Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.CreditReductionCourseImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getCreditReductionCourse()
		 * @generated
		 */
		EClass CREDIT_REDUCTION_COURSE = eINSTANCE.getCreditReductionCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION_COURSE__CODE = eINSTANCE.getCreditReductionCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Credit Reduction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION = eINSTANCE.getCreditReductionCourse_CreditReduction();

		/**
		 * The meta object literal for the '{@link WebPageCourses.impl.ScheduleImpl <em>Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see WebPageCourses.impl.ScheduleImpl
		 * @see WebPageCourses.impl.WebPageCoursesPackageImpl#getSchedule()
		 * @generated
		 */
		EClass SCHEDULE = eINSTANCE.getSchedule();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__DATE = eINSTANCE.getSchedule_Date();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__TIME = eINSTANCE.getSchedule_Time();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__ROOM = eINSTANCE.getSchedule_Room();

	}

} //WebPageCoursesPackage
